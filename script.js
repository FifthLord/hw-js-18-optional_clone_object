

let someObj = {
   name: "John",
   age: 30,
   table: {
      first: 1,
      second: 2,
      third: {
         first: "one",
         second: "two",
      },
      fourth: ["one", "two", 3],
   },
};


function deepClone(origin) {
   if (!Array.isArray(origin) && typeof origin !== 'object') {
      return origin;
   } else if (Array.isArray(origin)) {
      return origin.map((val) => {
         return deepClone(val)
      })
   } else if (typeof origin === 'object') {
      let clone = {}
      for (let key in origin) {
         clone[key] = deepClone(origin[key])
      }
      return clone
   }
}

let cloneObj = deepClone(someObj);


//---------test------//
cloneObj.name = "Pete";
cloneObj.table.second = "changed Two";

console.log((cloneObj));
console.log((exampleObj));